const Product = require("../models/Product");

module.exports.addProduct = (data) => {
	// User is an admin
	if (data.isAdmin) {
		// Creates a variable "newProduct" and instantiates a new "product" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newProduct = new product({
			name : data.product.name,
			description : data.product.description,
			price : data.product.productprice
		});
		// Saves the created object to our database
		return newProduct.save().then((product, error) => {
			// product creation successful
			if (error) {
				return false;
			// product creation failed
			} else {
				return true;
			};
		});
	// User is not an admin
	};

	let message = Promise.resolve(`User must be ADMIN to access this!`);
	return message.then((value) =>{
		return {value};
	});
};




// Retrieve ACTIVE product

module.exports.getAllActive  =() => {
	return Product.find({isAdmin:true }).then(result =>{
		return result;
	});
};

// Retrieve specific product
module.exports.getProduct = (reqParams)=>{
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};

// Update a Product

module.exports.updateProduct = (reqParams, reqBody) => {
	let updateProduct = {
 		name: reqBody.name,
 		description: reqBody.description,
 		price: reqBody.price,
 		isActive: reqBody.isActive,
 		createdOn: reqBody.createdOn,
 		orders: reqBody.orders
 	};

 	return Product.findByIdAndUpdate(reqParams.productId,
 		updateProduct).then((product, error) => {
 			if(error){
 				return false;
 			} else {
 				return true
 			};
 		});
	};
	//archive product

module.exports.archiveProduct = (reqParams) => {
	let updateActiveField = {
		isActive: false
	};
	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};