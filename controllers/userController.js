const User = require("../models/User");

const Product = require("../models/Product");

const bcrypt = require("bcrypt");

const auth = require("../auth")


module.exports.checkEmailExists= (reqBody) => { 
	return User.find({email: reqBody.email}).then(result =>{
		if (result.length > 0) {
			return true;
		} else {
			return false
		};
	});
};




// User Registration
/*
BUSINESS LOGIC
1.Create a new User object using the mongoose model and the information from the request body
2.Make sure that the password is encrypted
3.Save the new User to the database
*/

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,

	// "bcrypt.hashSync" is a function in the bcryptlibrary
	// that used the generate hah value for a given input string synchronously

	//"reqBody.password"-  inpput strig that needs to be hashed
	//10 - is value provided as the number of "salt"
	// rounds that the bcrypt-algorithm will rn in order to encrypt password	
		password: bcrypt.hashSync(reqBody.password, 10),

	mobileNumber: reqBody.phone,
	isAdmin : reqBody.isAdmin, 
	street: reqBody.street ,
	city: reqBody.city ,
	country : reqBody.country,
	orders: reqBody.orders
	});

	return newUser.save().then((user, error) =>{
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};
module.exports.loginUser = (reqBody) => {
 	return User.findOne({email: reqBody.email}).then (result =>
 		{

 			if(result == null){
 				return false
 			}else{
 				const isPasswordCorrect = bcrypt.compareSync(
 					reqBody.password, result.password);
 				if(isPasswordCorrect){
 					return{access: auth.createAccessToken(result)
 				}
 				}else{
 					return  false;
 				};
 			};
 		});
 };


//retrieve user details
module.exports.getProfile= (data) => {
	return User.findById(data.userId).then(result =>{
		result.password = "";
		return result;
	});
};

/*
Business Logic:
1. Find the document in the database using the user's ID
2. Add the product ID to the user's products array
3. Update the document in the MongoDB Atlas Database
*/

// Authenticated user products
module.exports.orders = async (data) => {
	let isUserUpdated = await User.findById(data.userId).
		then(User => {
		user.orders.push({productId: data.productId});
		return user.save().then((user, error) => {
			if (error){
				return false;
			} else {
				return true;
			};
		});
	});

// Add user ID in the orders array of the product
// isProductUpdated = true

	let isProductUpdated = await Product.findById(data.
		productId).then(product => {
			product.orders.push({userId: data.userId});
			return product.save().then((product, error) => {
				if (error){
					return false;
				}else {
					return true;
				};

			});
		})	;
		if(isUserUpdated && isProductUpdated){
			return true;
		} else {
			return false;
		};
};