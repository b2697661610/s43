const express = require("express");
const router = express.Router();

 const productController = require("../controllers/productController");

const auth = require("../auth");

router.post("/create", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all the products
router.get("/all" , (req, res) => {
	productController.getAllProduct().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving active product
router.get("/active" , (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving specific product
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});


// Route for updating a product
router.put("/:productId", (req,res) => {
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});



// Route to archiving a product
router.patch("/:productId/archive", auth.verify, (req, res) => {
	productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
});












	
module.exports = router;
